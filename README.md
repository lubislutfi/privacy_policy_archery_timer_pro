# Privacy Policy

_Last updated June 29, 2024_

This privacy notice for djalak putih ("we," "us," or "our") describes how and why we might collect, store, use, and/or share ("process") your information when you use our services ("Services"), such as when you:
- Download and use our mobile application (Archery Timer Pro)
- Engage with us in other related ways, including sales, marketing, or events

If you have questions or concerns, please contact us at lubislutfi@gmail.com.

## Summary of Key Points

- **Personal Information:** We may process personal information based on how you interact with our Services. [Learn more](#what-information-do-we-collect).
- **Sensitive Information:** We do not process sensitive personal information.
- **Third-Party Information:** We do not collect information from third parties.
- **Processing Information:** We process your information to provide, improve, and administer our Services, communicate with you, ensure security, and comply with the law. [Learn more](#how-do-we-process-your-information).
- **Sharing Information:** We may share information in specific situations and with specific third parties. [Learn more](#when-and-with-whom-do-we-share-your-personal-information).
- **Information Security:** We have measures to protect your personal information but cannot guarantee complete security. [Learn more](#how-do-we-keep-your-information-safe).
- **Your Rights:** Depending on your location, you may have rights regarding your personal information. [Learn more](#what-are-your-privacy-rights).
- **Exercising Your Rights:** Contact us to exercise your rights regarding your personal information. [Learn more](#how-do-you-exercise-your-rights).

## Table of Contents

1. [What Information Do We Collect?](#what-information-do-we-collect)
2. [How Do We Process Your Information?](#how-do-we-process-your-information)
3. [What Legal Bases Do We Rely On?](#what-legal-bases-do-we-rely-on)
4. [When and With Whom Do We Share Your Personal Information?](#when-and-with-whom-do-we-share-your-personal-information)
5. [Third-Party Websites](#what-is-our-stance-on-third-party-websites)
6. [Cookies and Tracking Technologies](#do-we-use-cookies-and-other-tracking-technologies)
7. [Social Logins](#how-do-we-handle-your-social-logins)
8. [Data Retention](#how-long-do-we-keep-your-information)
9. [Information Security](#how-do-we-keep-your-information-safe)
10. [Your Privacy Rights](#what-are-your-privacy-rights)
11. [Do-Not-Track Features](#controls-for-do-not-track-features)
12. [US Residents’ Privacy Rights](#do-united-states-residents-have-specific-privacy-rights)
13. [Updates to This Notice](#do-we-make-updates-to-this-notice)
14. [Contact Us](#how-can-you-contact-us-about-this-notice)
15. [Review, Update, or Delete Your Data](#how-can-you-review-update-or-delete-the-data-we-collect-from-you)

## What Information Do We Collect?

We collect personal information that you voluntarily provide to us when you:
- Register on the Services
- Express interest in our products and Services
- Participate in activities on the Services
- Contact us directly

## How Do We Process Your Information?

We process your personal information to:
- Provide, improve, and administer our Services
- Communicate with you
- Ensure security and prevent fraud
- Comply with legal obligations

## What Legal Bases Do We Rely On?

We process your personal information based on:
- Your consent
- Compliance with legal obligations
- Protection of vital interests

## When and With Whom Do We Share Your Personal Information?

We may share your information in the following situations:
- Business transfers (e.g., mergers, sales)
- Legal obligations (e.g., subpoenas, court orders)

## What Is Our Stance on Third-Party Websites?

We are not responsible for the safety of any information that you share with third parties that we may link to or who advertise on our Services.

## Do We Use Cookies and Other Tracking Technologies?

Yes, we may use cookies and similar tracking technologies to collect and store your information.

## How Do We Handle Your Social Logins?

If you choose to register or log in using a social media account, we may have access to certain information about you.

## How Long Do We Keep Your Information?

We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice unless otherwise required by law.

## How Do We Keep Your Information Safe?

We have implemented appropriate security measures to protect your personal information. However, no electronic transmission over the internet or information storage technology can be guaranteed to be 100% secure.

## What Are Your Privacy Rights?

Depending on your location, you may have rights such as:
- Access to your personal data
- Rectification or erasure of your personal data
- Restriction of processing your personal data

## Controls for Do-Not-Track Features

We do not currently respond to DNT signals as no uniform standard for recognizing and implementing DNT signals has been finalized.

## Do United States Residents Have Specific Privacy Rights?

Yes, if you are a resident of certain US states, you may have rights such as access to and deletion of your personal data.

## Do We Make Updates to This Notice?

Yes, we may update this privacy notice as necessary to stay compliant with relevant laws.

## How Can You Contact Us About This Notice?

If you have questions or comments about this notice, you may email us at lubislutfi@gmail.com or contact us by post at:

djalak putih  
buleleng  
buleleng, bali 61453  
Indonesia

## How Can You Review, Update, or Delete the Data We Collect From You?

To request to review, update, or delete your personal information, please contact us at lubislutfi@gmail.com.
